class SinglyLinkedList:
    """ Simple singly linked list realization

    """
    class Node:
        def __init__(self, value, next_node=None):
            self.value = value
            self.next_node = next_node

        def __str__(self):
            return str(self.value)

    def __init__(self):
        self.__len = 0
        self.__head = None
        self.__tail = None

    def __len__(self):
        return self.__len

    def __str__(self):
        string = 'LinkedList: [ head'
        current = self.__head

        while current is not None:
            string += ' -> {0}'.format(current)
            current = current.next_node

        return string + ' ]'

    def add(self, value):
        if self.__head is None:
            self.__head = self.__tail = SinglyLinkedList.Node(value=value)
        else:
            self.__tail.next_node = SinglyLinkedList.Node(value=value)
            self.__tail = self.__tail.next_node

        self.__len += 1

    def repack(self):
        """ Repack LinkedList
        time complexity - O(n^2)

         original LinkedList: 1->2->3->4->5->6->7->8
        :return: LinkedList: 1->8->2->7->3->6->4->5
        """
        middle = self.__head
        count = 0

        while count < self.__len / 2:
            middle = middle.next_node
            count += 1

        current = self.__head

        while current != middle and current.next_node is not self.__tail:
            next_node = current.next_node
            variable_prt = middle

            while variable_prt.next_node is not self.__tail:
                variable_prt = variable_prt.next_node

            current.next_node = variable_prt.next_node
            variable_prt.next_node.next_node = next_node

            variable_prt.next_node = None
            self.__tail = variable_prt

            current = next_node

        return self
