#!/usr/bin/env python2

from task12 import split_vector, factorization


if __name__ == '__main__':

    def task1():
        print '===task 1==='

        multipliers = factorization(n=1000000)

        for i, m in enumerate(multipliers, start=0):
            print '{0} = {1}'.format(
                i + 1, ' * '.join(map(str, sorted(m))))


    def task2():
        print '===task 2==='

        parts = split_vector(n=100, m=21)
        print parts


    def task3():
        import sys
        from option_parser import OptionParser

        option_parser = OptionParser(
            names_list=['a=', 'b=', 'help', 'task1', 'task2', 'task4'],
            is_necessary_list=[True] + [False] * 6)

        options = option_parser.parse_args(sys.argv)

        a = int(options['--a'])
        b = None
        if '--b' in options.keys():
            b = int(options['--b'])

        print 'a = {0}, b = {1}'.format(a, b)

        if '--task1' in options.keys():
            task1()
        if '--task2' in options.keys():
            task2()
        if '--task4' in options.keys():
            task4()

        print options


    def task4():
        print '===task 4==='

        from linked_list import SinglyLinkedList

        ll = SinglyLinkedList()
        for i in xrange(100):
                ll.add(i)

        print ll
        print ll.repack()


    task3()
