import getopt


class OptionParser:
    """Simple OptionParser class

    """
    def __init__(self, names_list, is_necessary_list):
        """

        :param names_list: list of options names
        :param is_necessary_list: list of bool values:
        if is_necessary_list[i] == True - names_list[i] is necessary
        """
        self.__names = names_list
        self.__is_necessary = is_necessary_list

    def parse_args(self, args):
        """

        :param args: list of arguments -> sys.argv
        :return: dictionary of options
        """
        sk_name = args[0]
        opt_list = reduce(lambda x, y: x + y,
                          [s.split('=') for s in args[1:]])

        try:
            opts, args = getopt.getopt(opt_list, [], self.__names)
            options = dict(opts)

            if '--help' in options.keys():
                self.print_help(sk_name=sk_name)

                options.pop('--help', None)

            for i, name in enumerate(self.__names):
                if self.__is_necessary[i] and '--' + name[:-1] not in options.keys():
                    assert False, 'option "{0}" not given'.format(name)

        except getopt.GetoptError as err:
            assert False, str(err)

        return options

    def print_help(self, sk_name='main.py'):
        print 'Usage: {0} options [arguments]\n\nOptions:'.format(sk_name)

        for i, name in enumerate(self.__names):
            form = '\t{0}' if self.__is_necessary[i] else '\t[ {0} ]'

            if name[-1] == '=':
                print form.format('--{0}=value, --{0} VALUE'.format(name[:-1]))
            else:
                print form.format('--{0}'.format(name))
