def factorization(n):
    """Integer factorization method

    :param n:
    :return: list of multipliers as multipliers[i] -
        multipliers for number i (1 <= i <= n)
    """
    multipliers = [[]] * n
    for i in xrange(1, n):
        multipliers[i] = [i + 1]

    prime_numbers = [2, 3]

    for i in xrange(3, n):
        k = i + 1
        n_sqrt = int(n ** 0.5) + 1

        for d in prime_numbers:
            if d > n_sqrt:
                break

            elif k % d == 0:
                # using previously calculated multipliers
                multipliers[i] = multipliers[d - 1] + multipliers[k / d - 1]
                break

        if len(multipliers[i]) == 1:
            prime_numbers.append(k)

    multipliers[0] = [1]
    return multipliers


def split_vector(n, m):
    """Vector split into equal parts

    :param n: the length of the vector
    :param m: the numbers of parts
    :return: list of tuples as [(begin_1, end_1),..,(begin_m,..,end_m)]
        where begin_i - the index of the beginning part_i
        end_i - the index of the ending part_i
    """
    mod = n % m
    border1 = mod / 2
    border2 = mod - border1
    part_len = n / m

    return [(i, i + part_len - 1) for i in range(border1, n - border2, part_len)]
